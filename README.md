# Releases

# v2.0
+ Completely skip the initial setup screen
*enabling this feature will modify os0:kd/registry.db0.*

VPK: https://bitbucket.org/SilicaAndPina/fastreset/downloads/fastreset-2.0.vpk  

# v1.0 
A tool to make restoring your PSVITA alot faster by replacing the 1 minute long unskippable intro video with one thats 0:00 long, this video is written to ux0: so you will need your memory card inserted when on the inital setup screen for this to work
  
VPK: https://bitbucket.org/SilicaAndPina/fastreset/downloads/fastreset-1.0.vpk  


# Readme

# fastreset v 2.0
Will completly skip initalsetup.self by modifying the default value of the 'initalize' key in os0:kd/registry.db0

# fastreset v 1.0
Will skip the intro video by writing a fake update for welcome park to ux0:patch that replaces the intro video  with a 0:00 mp4
